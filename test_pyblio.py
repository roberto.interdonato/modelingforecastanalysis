import pandas as pd


from pybliometrics.scopus import AbstractRetrieval, AuthorRetrieval, ScopusSearch

#ab = AbstractRetrieval("10.1016/j.softx.2019.100263")
#print(ab.title)
#print(ab.publicationName)


#s = ScopusSearch('TITLE(lurker OR lurking OR {silent user})')


#s = ScopusSearch('TITLE(prospective OR anticipation OR foresights OR forecast OR {future studies} OR {prospective thinking} OR {strategic planning} OR prospeccion OR prospectiva) AND TITLE(modélisation OR modelling OR {scenario planning} OR {scenario mapping} OR simulation OR {quantitative assessement} OR {qualitative assessment} OR {narrative scenarios} OR modelado)')

s = ScopusSearch('TITLE-ABS-KEY(prospective OR anticipation OR foresights OR forecast OR {future studies} OR {prospective thinking} OR {strategic planning} OR prospeccion OR prospectiva) AND TITLE-ABS-KEY(modélisation OR modelling OR {scenario planning} OR {scenario mapping} OR simulation OR {quantitative assessement} OR {qualitative assessment} OR {narrative scenarios} OR modelado)')


print(s.get_results_size())
df = pd.DataFrame(pd.DataFrame(s.results))
df.to_csv('test_jeremy1_abs-title-key.csv', sep=';')