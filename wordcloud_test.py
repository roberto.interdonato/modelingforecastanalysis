import numpy as np
import pandas as pd
from os import path
from PIL import Image
import wordcloud
import matplotlib.pyplot as plt
import string
import datetime
from nltk.corpus import stopwords
import glob
from os.path import basename

group1 = ['prospective', 'anticipation', 'foresights', 'forecast', 'future studies', 'prospective thinking', 'strategic planning', 'prospeccion', 'prospectiva']
group2 = ['modélisation', 'modelling', 'scenario', 'planning', 'mapping', 'simulation', 'quantitative','assessement', 'qualitative', 'narrative','scenarios','modelado']


def gen_wordcloud(input_f,outname):
    #input_f = "global_occs_ngrams_abs-title-key_1970_2020_noGroupWords.csv"
    df = pd.read_csv(input_f,sep=';')
    #print(df)
    wdict = dict()
    for index, row  in df.iterrows():
        text=row[0].strip().translate(str.maketrans('', '', string.punctuation))
        wdict[text]=float(row[1])

    # Create and generate a word cloud image:
    wc = wordcloud.WordCloud(width=1000,height=500,background_color="white").generate_from_frequencies(wdict)
    #wc = wordcloud.WordCloud(width=1000, height=500, background_color="black").generate_from_frequencies(wdict)
    # Display the generated image:
    #plt.imshow(wc, interpolation='bilinear')
    #plt.axis("off")
    #plt.show()

    wc.to_file(outname)

files=glob.glob('./global_occs_ngrams_*.csv')

for f in files:
    print(f)
    gen_wordcloud(f,basename(f).split('.')[0]+'.png')
