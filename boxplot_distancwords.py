import numpy as np
import pandas as pd
from os import path
from PIL import Image
import wordcloud
import matplotlib.pyplot as plt
import string
import datetime
from nltk.corpus import stopwords
import glob
from os.path import basename
import matplotlib.pyplot as plt

def gen_boxplot(input_f,outname):
    df = pd.read_csv(input_f, sep=';')
    vals = df.iloc[:,1]

    # Create a figure instance
    fig = plt.figure(1, figsize=(9, 6))

    # Create an axes instance
    ax = fig.add_subplot(111)

    # Create the boxplot
    bp = ax.boxplot(vals)

    # Save the figure
    fig.savefig(outname, bbox_inches='tight')
    fig.clear()

files=glob.glob('./distance_words_*.csv')

for f in files:
    print(f)
    gen_boxplot(f,basename(f).split('.')[0]+'.png')
